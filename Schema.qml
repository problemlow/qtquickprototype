import QtQuick 2.11
import QtQuick.Window 2.11

Item {
    Rectangle {
        id: colourMain
        x: 0
        y: 0
        width: 0
        height: 0
        color: "#000000"

        Rectangle {
            id: colourSecondary
            x: 0
            y: 0
            width: 0
            height: 0
            color: "#ffffff"
        }
    }

    Rectangle {
        id: colourTertiary
        x: 0
        y: 0
        width: 0
        height: 0
        color: "#777777"
    }

}

