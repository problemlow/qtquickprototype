import QtQuick 2.11
import QtQuick.Controls 1.4
import QtQuick.Window 2.11
// Login Screen WINDOW

Window {
    id: keyAddView
    visible: true
    width: 1280
    height: 720
    title: qsTr("Prototype")

    Rectangle {
        id: barTop
        y: 0
        width: parent.width
        height: parent.height/32*5
        color: "#000000"

        Rectangle {
            id: boxPageName
            x: parent.height/2-height/2
            y: parent.height/2-height/2
            width: parent.width/3
            height: parent.height*.5
            color: "#ffffff"
            border.width: parent.height/32
            border.color: "#444444"

            Text {
                id: text1
                x: parent.width/2-width/2
                y: parent.height*.05
                width: parent.width-parent.height*.1
                height: parent.height*.9
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                text: qsTr("Key Add View")
                font.pixelSize: parent.height/2
            }
        }
    }

    Rectangle {
        id: barBottom
        x: 0
        y: parent.height/32*27
        width: parent.width
        height: parent.height/32*5
        color: "#000000"

        Button {
            id: buttonAddPerson
            x: parent.height/2-height/2
            y: parent.height/2-height/2
            width: parent.width/5
            height: parent.height*.5
            text: qsTr("Add/Update Person")
        }

        Button {
            id: buttonNotes
            x: parent.width/64*10+width/2
            y: parent.height/2-height/2
            width: parent.width/5
            height: parent.height*.5
            text: qsTr("View Notes")
        }

        Button {
            id: buttonBack
            x: parent.width/64*57-width/2
            y: parent.height/2-height/2
            width: parent.width/5
            height: parent.height*.5
            text: qsTr("Back")
            MouseArea {
                anchors.fill: parent
                Loader { id: pageLoader }
                onClicked: {keyAddView.close(); pageLoader.source = "generalOverview.qml";}
            }
        }

        Button {
            id: buttonDelete
            x: parent.width/64*43-width/2
            y: parent.height/2-height/2
            width: parent.width/5
            height: parent.height*.5
            text: qsTr("View Delete")
        }
    }

    Rectangle {
        id: textNotes
        x: parent.width*.75-width/2
        y: parent.height/2-height/2
        width: parent.width/2-parent.height/32
        height: parent.height/32*20
        color: "#ffffff"
        border.width: parent.height/32
        border.color: "#444444"

        TextInput {
            id: textInputNotes
            x: parent.width/2-width/2
            y: parent.height*.05
            width: parent.width-parent.height*.1
            height: parent.height*.9
            color: "#000000"
            text: qsTr("Notes")
            font.pixelSize: textInputNotes.height
            font.capitalization: Font.AllUppercase
            font.family: "Verdana"
            echoMode: TextInput.Normal
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
    }

    Rectangle {
        id: boxKeyID
        x: parent.width/64
        y: parent.height/64*12
        width: parent.width/5
        height: parent.height/64*5
        color: "#ffffff"
        border.width: barTop.height/32
        border.color: "#444444"

        Text {
            id: textKeyID
            x: parent.width/2-width/2
            y: parent.height*.05
            width: parent.width-parent.height*.1
            height: parent.height*.9
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: qsTr("Key ID")
            font.pixelSize: parent.height/2
        }
    }

    Rectangle {
        id: boxKeyType
        x: parent.width/64
        y: parent.height/64*18
        width: parent.width/5
        height: parent.height/64*5
        color: "#ffffff"
        border.width: barTop.height/32
        border.color: "#444444"

        Text {
            id: textKeyType
            x: parent.width/2-width/2
            y: parent.height*.05
            width: parent.width-parent.height*.1
            height: parent.height*.9
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: qsTr("Key Type")
            font.pixelSize: parent.height/2
        }
    }

    Rectangle {
        id: boxKeyRoom
        x: parent.width/64
        y: parent.height/64*24
        width: parent.width/5
        height: parent.height/64*5
        color: "#ffffff"
        border.width: barTop.height/32
        border.color: "#444444"

        Text {
            id: textKeyRoom
            x: parent.width/2-width/2
            y: parent.height*.05
            width: parent.width-parent.height*.1
            height: parent.height*.9
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: qsTr("Key Room")
            font.pixelSize: parent.height/2
        }
    }

    Rectangle {
        id: boxKeyHolder
        x: parent.width/64
        y: parent.height/64*30
        width: parent.width/5
        height: parent.height/64*5
        color: "#ffffff"
        border.width: barTop.height/32
        border.color: "#444444"

        Text {
            id: textKeyHolder
            x: parent.width/2-width/2
            y: parent.height*.05
            width: parent.width-parent.height*.1
            height: parent.height*.9
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: qsTr("Current Holder")
            font.pixelSize: parent.height/2
        }
    }

    Rectangle {
        id: boxKeyStorageLocation
        x: parent.width/64
        y: parent.height/64*36
        width: parent.width/5
        height: parent.height/64*5
        color: "#ffffff"
        border.width: barTop.height/32
        border.color: "#444444"

        Text {
            id: textKeyStorageLocation
            x: parent.width/2-width/2
            y: parent.height*.05
            width: parent.width-parent.height*.1
            height: parent.height*.9
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: qsTr("Storage Location")
            font.pixelSize: parent.height/2
        }
    }

    Rectangle {
        id: boxKeyAllocationPeriods
        x: parent.width/64
        y: parent.height/64*42
        width: parent.width/5
        height: parent.height/64*5
        color: "#ffffff"
        border.width: barTop.height/32
        border.color: "#444444"

        Text {
            id: textKeyAllocationPeriods
            x: parent.width/2-width/2
            y: parent.height*.05
            width: parent.width-parent.height*.1
            height: parent.height*.9
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: qsTr("Allocation Periods")
            font.pixelSize: parent.height/2
        }
    }

    Rectangle {
        id: boxKeyIDData
        x: parent.width/64*15
        y: parent.height/64*12
        width: parent.width/5
        height: parent.height/64*5
        color: "#ffffff"
        border.width: barTop.height/32
        border.color: "#444444"

        Text {
            id: textKeyIDData
            x: parent.width/2-width/2
            y: parent.height*.05
            width: parent.width-parent.height*.1
            height: parent.height*.9
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: qsTr("AutoNumber")
            font.pixelSize: parent.height/2
        }
    }

    Rectangle {
        id: boxKeyTypeData
        x: parent.width/64*15
        y: parent.height/64*18
        width: parent.width/5
        height: parent.height/64*5
        color: "#ffffff"
        border.width: barTop.height/32
        border.color: "#444444"

        TextInput {
            id: textKeyTypeData
            x: parent.width/2-width/2
            y: parent.height*.05
            width: parent.width-parent.height*.1
            height: parent.height*.9
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: qsTr("NCF Fob")
            font.pixelSize: parent.height/2
        }
    }

    Rectangle {
        id: boxKeyRoomData
        x: parent.width/64*15
        y: parent.height/64*24
        width: parent.width/5
        height: parent.height/64*5
        color: "#ffffff"
        border.width: barTop.height/32
        border.color: "#444444"

        TextInput {
            id: textKeyRoomData
            x: parent.width/2-width/2
            y: parent.height*.05
            width: parent.width-parent.height*.1
            height: parent.height*.9
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: qsTr("NX01")
            font.pixelSize: parent.height/2
        }
    }

    Rectangle {
        id: boxKeyHolderData
        x: parent.width/64*15
        y: parent.height/64*30
        width: parent.width/5
        height: parent.height/64*5
        color: "#ffffff"
        border.width: barTop.height/32
        border.color: "#444444"

        TextInput {
            id: textKeyHolderData
            x: parent.width/2-width/2
            y: parent.height*.05
            width: parent.width-parent.height*.1
            height: parent.height*.9
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: qsTr("Persons Name")
            font.pixelSize: parent.height/2
        }
    }

    Rectangle {
        id: boxKeyStorageLocationData
        x: parent.width/64*15
        y: parent.height/64*36
        width: parent.width/5
        height: parent.height/64*5
        color: "#ffffff"
        border.width: barTop.height/32
        border.color: "#444444"

        TextInput {
            id: textKeyStorageLocationData
            x: parent.width/2-width/2
            y: parent.height*.05
            width: parent.width-parent.height*.1
            height: parent.height*.9
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: qsTr("Locker Number")
            font.pixelSize: parent.height/2
        }
    }

    Rectangle {
        id: boxKeyAllocationPeriodsData
        x: parent.width/64*15
        y: parent.height/64*42
        width: parent.width/5
        height: parent.height/64*5
        color: "#ffffff"
        border.width: barTop.height/32
        border.color: "#444444"

        TextInput {
            id: textKeyAllocationPeriodsData
            x: parent.width/2-width/2
            y: parent.height*.05
            width: parent.width-parent.height*.1
            height: parent.height*.9
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: qsTr("XX:XX-XX:XX")
            font.pixelSize: parent.height/2
        }
    }
}
