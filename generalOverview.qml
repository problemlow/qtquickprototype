import QtQuick 2.11
import QtQuick.Controls 1.4
import QtQuick.Window 2.11

// General Overview WINDOW

Window {
    id: generalOverview
    visible: true
    width: 1280
    height: 720
    title: qsTr("Prototype")

    Rectangle {
        id: barTop
        y: 0
        width: parent.width
        height: parent.height/32*5
        color: "#000000"

        Rectangle {
            id: boxPageName
            x: parent.height/2-height/2
            y: parent.height/2-height/2
            width: parent.width/3
            height: parent.height*.5
            color: "#ffffff"
            border.width: parent.height/32
            border.color: "#444444"

            Text {
                id: text1
                x: parent.width/2-width/2
                y: parent.height*.05
                width: parent.width-parent.height*.1
                height: parent.height*.9
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                text: qsTr("General Overview")
                font.pixelSize: parent.height/2
            }
        }

        Rectangle {
            id: txtBoxSearch
            x: parent.width-width-(parent.height/2-height/2)
            y: parent.height/2-height/2
            width: parent.width/3
            height: parent.height*.5
            color: "#ffffff"
            border.width: parent.height/32
            border.color: "#444444"

            TextInput {
                id: textInput
                x: parent.width/2-width/2
                y: parent.height*.05
                width: parent.width-parent.height*.1
                height: parent.height*.9
                color: "#808080"
                text: qsTr("Search")
                font.family: "Verdana"
                echoMode: TextInput.Normal
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: parent.height/2
            }
        }

        ComboBox {
            id: comboBox
            x: parent.width/32*21-width-(parent.height/2-height/2)
            y: parent.height/2-height/2
            width: parent.width/6
            height: parent.height*.5
            model: ["Select Colomn", "col1", "col2", "col3"]
        }
    }

    Rectangle {
        id: barBottom
        x: 0
        y: parent.height/32*27
        width: parent.width
        height: parent.height/32*5
        color: "#000000"

        Rectangle {
            id: txtBoxSearchResult
            x: parent.width-width-(parent.height/2-height/2)
            y: parent.height/2-height/2
            width: parent.width/3
            height: parent.height*.5
            color: "#ffffff"
            border.width: parent.height/32
            border.color: "#444444"
            Text {
                id: text2
                x: parent.width/2-width/2
                y: parent.height*.05
                width: parent.width-parent.height*.1
                height: parent.height*.9
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                text: qsTr("No. of Search Results")
                font.pixelSize: parent.height/2

            }
        }
        Button {
            id: boxPageName1
            x: parent.height/2-height/2
            y: parent.height/2-height/2
            width: parent.width/3
            height: parent.height*.5
            text: qsTr("Add Key")
            Loader { id: pageLoader }
            onClicked: {generalOverview.close(); pageLoader.source = "keyAddView.qml";}
        }
    }

    TableView {
        id: tableView
        x: parent.width/2-width/2
        y: parent.height/2-height/2
        width: parent.width-parent.height/32*2
        height: parent.height/32*20
        TableViewColumn {
            role: "Key ID" // was in the example
            title: "Key ID" // the thing that actually shows up
            width: tableView.width/6 // using parent doesnt work for some reason
        }
        TableViewColumn {
            role: "Room Number" // was in the example
            title: "Room Number" // the thing that actually shows up
            width: tableView.width/6 // using parent doesnt work for some reason
        }
        TableViewColumn {
            role: "Key Type" // was in the example
            title: "Key Type" // the thing that actually shows up
            width: tableView.width/6 // using parent doesnt work for some reason
        }
        TableViewColumn {
            role: "Current Holder" // was in the example
            title: "Current Holder" // the thing that actually shows up
            width: tableView.width/6 // using parent doesnt work for some reason
        }
        TableViewColumn {
            role: "Storage Location" // was in the example
            title: "Storage Location" // the thing that actually shows up
            width: tableView.width/6 // using parent doesnt work for some reason
        }
        TableViewColumn {
            role: "Allocation Periods" // was in the example
            title: "Allocation Periods" // the thing that actually shows up
            width: tableView.width/6 // using parent doesnt work for some reason
        }
    }
}
